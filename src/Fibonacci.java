import java.util.Scanner;

public class Fibonacci {
	
	public static void main (String[] args){
		int lengthCount;
		int printIndex = 0;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println ("This program will display as many numbers as you want \nin the Fibonacci sequence");
		System.out.println ("Please enter the length of the sequence you wish to see");
		
		int lengthIn = input.nextInt();
		
		if (lengthIn < 3){
			
			lengthCount = 3;	
		}
		
		else lengthCount = lengthIn;
		
		int[] fSequence = new int[lengthCount] ;
		
		
		
		fSequence[0] = 0;
		fSequence[1] = 1;
		fSequence[2] = 1;
		
		
		for (int count = 3; lengthCount >count; count++) {
			
		
			
			fSequence[count] = fSequence[(count - 1)] + fSequence[(count - 2)];
			
			
		}
		
		while (printIndex < lengthIn){
			
			System.out.println (fSequence[printIndex]);
			
			printIndex++;
		}
			
			
		
		
		
	}

}
